<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
// $routes->get('/', 'Home::index');
$routes->get('/', 'MyController::index',['filter' => 'regisGuard']);
$routes->post('/auth', 'MyController::loginAuth');
$routes->get('/signup', 'MyController::register',['filter' => 'regisGuard']);
$routes->post('/insert', 'MyController::insert');
$routes->get('/signout', 'MyController::logout');
$routes->get('/home', 'MyController::home',['filter' => 'authGuard']);
$routes->get('/pegawai', 'MyController::pegawai',['filter' => 'authGuard']);
$routes->get('/pegawai/edit/(:num)', 'MyController::editPegawai/$1',['filter' => 'authGuard']);
$routes->post('/pegawai/update', 'MyController::updatePegawai',['filter' => 'authGuard']);
$routes->get('/pegawai/(:num)', 'MyController::deletePegawai/$1',['filter' => 'authGuard']);
$routes->get('/presensi', 'MyController::presensi',['filter' => 'authGuard']);
$routes->get('/presensi/(:alpha)', 'MyController::presensi/$1',['filter' => 'authGuard']);
$routes->get('/activity', 'MyController::activity',['filter' => ['authGuard', \App\Filters\AdminOnly::class]]);

$routes->get('/u/(:alpha)', 'MyController::userMenu/$1',['filter' => 'authGuard']);

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
