<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
 
class User_model extends Model
{
    protected $table = 'user';
 
    public function getUser($id = false)
    {
        if ($id === false) {
            return $this
            ->where('delete_mark =', null)
            ->findAll();
        } else {
            return $this
            ->where('id_user', $id)
            ->where('delete_mark =', null)
            ->first();
        }
    }
 
    public function insertUser($data)
    {
        $builder = $this->db->table($this->table);
        return $builder->insert($data);
    }

    public function updateUser($data, $id)
    {
        $builder = $this->db->table($this->table);
        $builder->where('id_user', $id);
        return $builder->update($data);
    }

    public function deleteUser($id)
    {
        $builder = $this->db->table($this->table);
        $builder->where('id_user', $id);
        return $builder->update(['delete_mark' => 1]);
    }
}