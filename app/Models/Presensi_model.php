<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
 
class Presensi_model extends Model
{
    protected $table = 'presensi';
 
    public function getData($id = false)
    {
        if ($id === false) {
            return $this->db->query("SELECT presensi.id_user, user.nama_user, presensi.keterangan, presensi.tanggal, presensi.created_at FROM presensi INNER JOIN user ON presensi.id_user=user.id_user ORDER BY tanggal DESC, created_at DESC")->getResultArray();
        } else {
            return $this->where('id_user', $id)->findAll();
        }
    }

    public function getToday($id = false)
    {
        $today = date('Y-m-d');
        if ($id === false) {
            return $this->where('tanggal', $today)->findAll();
        } else {
            return $this->where('tanggal', $today)->where('id_user', $id)->findAll();
        }
    }
 
    public function insertData($data)
    {
        $builder = $this->db->table($this->table);
        return $builder->insert($data);
    }
}