<?php
 
namespace App\Models;
 
use CodeIgniter\Model;
 
class userActivity_model extends Model
{
    protected $table = 'user_activity';
 
    public function getData($id = false)
    {
        if ($id === false) {
            return $this->orderBy("create_date", "desc")->findAll(100);
        } else {
            return $this->where('no_activity', $id)->first();
        }
    }
 
    public function insertData($data)
    {
        $builder = $this->db->table($this->table);
        return $builder->insert($data);
    }
}