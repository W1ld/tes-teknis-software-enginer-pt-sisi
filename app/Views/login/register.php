<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.104.2">
    <title><?= $title ?></title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/sign-in/">

    

    

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" crossorigin="anonymous">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      .b-example-divider {
        height: 3rem;
        background-color: rgba(0, 0, 0, .1);
        border: solid rgba(0, 0, 0, .15);
        border-width: 1px 0;
        box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
      }

      .b-example-vr {
        flex-shrink: 0;
        width: 1.5rem;
        height: 100vh;
      }

      .bi {
        vertical-align: -.125em;
        fill: currentColor;
      }

      .nav-scroller {
        position: relative;
        z-index: 2;
        height: 2.75rem;
        overflow-y: hidden;
      }

      .nav-scroller .nav {
        display: flex;
        flex-wrap: nowrap;
        padding-bottom: 1rem;
        margin-top: -1px;
        overflow-x: auto;
        text-align: center;
        white-space: nowrap;
        -webkit-overflow-scrolling: touch;
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="/assets/dist/css/signin.css" rel="stylesheet">
  </head>
  <body class="text-center">
    
<main class="form-signin w-100 m-auto container">
  <form action="/insert" method="post">
    <img class="mb-4" src="/assets/brand/bootstrap-logo.svg" alt="" width="72" height="57">

    <div class="row">
      <div class="form-floating col-12">
        <input type="text" class="form-control" id="nama" placeholder="nama" name="nama" required>
        <label for="nama">Nama Lengkap</label>
      </div>
    </div>
    <div class="row">
      <div class="form-floating col-6">
        <input type="text" class="form-control" id="username" placeholder="username" name="username" required>
        <label for="username">Username</label>
      </div>
      <div class="form-floating col-6">
        <input type="text" class="form-control" id="hp" placeholder="Nomor HP" name="hp" required>
        <label for="hp">Nomor HP</label>
      </div>
    </div>
    <div class="row">
      <div class="form-floating col-6">
        <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
        <label for="password">Password</label>
      </div>
      <div class="form-floating col-6">
        <input type="text" class="form-control" id="wa" placeholder="wa" name="wa" required>
        <label for="wa">Nomor WA</label>
      </div>
    </div>
    <div class="row">
      <div class="form-floating col-6">
        <input type="email" class="form-control" id="email" placeholder="email" name="email" required>
        <label for="email">Email</label>
      </div>
      <div class="form-floating col-6">
        <input type="text" class="form-control" id="pin" placeholder="PIN" name="pin" required>
        <label for="pin">PIN</label>
      </div>
    </div>
    <button class="w-100 btn btn-lg btn-primary" type="submit">Sign up</button>
  </form>
  <div class="mt-3">
    <a href="/"><h6>Sign in</h6></a>
  </div>
</main>


    
  </body>
</html>
