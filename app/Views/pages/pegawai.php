<?= $this->extend('header-footer/index') ?>

<?= $this->section('nama');
echo $_SESSION['level'];
$this->endSection() ?>
<?= $this->section('title');
echo 'Pegawai';
$this->endSection() ?>
<?= $this->section('content') ?>
<div class="container pt-5">
        <div class="text-end"><?php if($_SESSION['level'] == "admin"){?>
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal" data-whatever="@getbootstrap" style="margin-bottom:10px;">Tambah Pegawai</button>
        <?php }?></div>
 
        <div class="card">
            <div class="card-header bg-primary text-white">
                <h4 class="card-title" style="text-align: center;">Data Pegawai</h4>
            </div>
 
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Pegawai</th>
                                <th>Email</th>
                                <th>HP</th>
                                <th>WA</th>
                                <th>Jabatan</th>
                                <?php if($_SESSION['level'] == "admin"){?>
                                <th>Aksi</th>
                                <?php }?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($users as $krywn) { ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $krywn['nama_user']; ?></td>
                                    <td><?= $krywn['email']; ?></td>
                                    <td><?= $krywn['no_hp']; ?></td>
                                    <td><?= $krywn['wa']; ?></td>
                                    <td><?= $krywn['jabatan']; ?></td>
                                    <?php if($_SESSION['level'] == "admin"){?>
                                    <td>
                                        <a href="<?= base_url('pegawai/edit/' . $krywn['id_user']); ?>" class="btn btn-success" data-target="#editModal">
                                            Edit</a>
                                        <?php if($krywn['id_user'] !== $_SESSION['id_user']){?>
                                        <a href="<?= base_url('pegawai/' . $krywn['id_user']); ?>" onclick="javascript:return confirm('Apakah Anda yakin ingin menghapus data karyawan?')" class="btn btn-danger">
                                            Hapus</a>
                                        <?php }?>
                                    </td>
                                    <?php }?>
                                </tr>
                            <?php $no++;
                            } ?>
                        </tbody>
 
                    </table>
                </div>
            </div>
        </div>
    </div>
 
    <!--   Modal Tambah Data-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Pegawai</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <form action="/insert" method="post">
                        <div class="form-floating">
                            <input type="text" class="form-control" id="nama" placeholder="nama" name="nama" required>
                            <label for="nama">Nama Lengkap</label>
                        </div>
                        <div class="form-floating mt-2">
                            <input type="text" class="form-control" id="username" placeholder="username" name="username" required>
                            <label for="username">Username</label>
                        </div>
                        <div class="form-floating mt-2">
                            <input type="text" class="form-control" id="hp" placeholder="Nomor HP" name="hp" required>
                            <label for="hp">Nomor HP</label>
                        </div>
                        <div class="form-floating mt-2">
                            <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
                            <label for="password">Password</label>
                        </div>
                        <div class="form-floating mt-2">
                            <input type="text" class="form-control" id="wa" placeholder="wa" name="wa" required>
                            <label for="wa">Nomor WA</label>
                        </div>
                        <div class="form-floating mt-2">
                            <input type="email" class="form-control" id="email" placeholder="email" name="email" required>
                            <label for="email">Email</label>
                        </div>
                        <div class="form-floating mt-2">
                            <input type="text" class="form-control" id="pin" placeholder="PIN" name="pin" required>
                            <label for="pin">PIN</label>
                        </div>
                        <div class="form-floating mt-2">
                            <input type="text" class="form-control" id="pin" placeholder="jabatan" name="jabatan" required>
                            <label for="pin">Jabatan</label>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>