<?= $this->extend('header-footer/index') ?>

<?= $this->section('nama');
echo $_SESSION['level'];
$this->endSection() ?>
<?= $this->section('title');
echo 'Presensi';
$this->endSection() ?>
<?= $this->section('content') ?>
<div class="container pt-5">
        <?php if(session()->getFlashdata('msg')):?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <span><?= session()->getFlashdata('msg') ?></span>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    <?php endif;?>
    <div class="card mb-3">
        <div class="row p-3">
            <?php if(count($today) == 0){?>
            <div class="col-md-6 col-sm-12">
                <h6 class="mt-2">Anda belum melakukan cek kehadiran, silahkan cek kehadiran anda!</h6>
            </div>
            <div class="col-md-6 col-sm-12 text-end">
                <div class="row">
                    <div class="col-4"></div>
                    <div class="col-2"><a href="<?= base_url('presensi/hadir')?>" onclick="javascript:return confirm('Apakah Anda yakin mengisi kehadiran dengan status \'Hadir\'?')" class="w-100 btn btn-success">Hadir</a></div>
                    <div class="col-2"><a href="<?= base_url('presensi/izin')?>" onclick="javascript:return confirm('Apakah Anda yakin mengisi kehadiran dengan status \'Izin\'?')" class="w-100 btn btn-primary">Izin</a></div>
                    <div class="col-2"><a href="<?= base_url('presensi/telat')?>" onclick="javascript:return confirm('Apakah Anda yakin mengisi kehadiran dengan status \'Telat\'?')" class="w-100 btn btn-warning">Telat</a></div>
                    <div class="col-2"><a href="<?= base_url('presensi/absen')?>" onclick="javascript:return confirm('Apakah Anda yakin mengisi kehadiran dengan status \'Absen\'?')" class="btn btn-danger">Absen</a></div>
                </div>
            </div>
            <?php }else{?>
            <div class="col-12 text-center">
                <h5 class="mt-2">Anda sudah melakukan cek kehadiran.</h5>
            </div>
            <?php }?>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h4>Data Presensi Hari ini</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Pegawai</th>
                            <th>Keterangan</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($user as $krywn) { ?>
                            <tr>
                                <td><?= $no; ?></td>
                                <td><?= $krywn['nama_user']; ?></td>
                                <td><?= $krywn['keterangan']; ?></td>
                                <td><?= date('d-m-Y',strtotime($krywn['tanggal'])); ?></td>
                            </tr>
                        <?php $no++;
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>