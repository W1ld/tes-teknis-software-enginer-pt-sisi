<?= $this->extend('header-footer/index') ?>

<?= $this->section('nama');
echo $_SESSION['level'];
$this->endSection() ?>
<?= $this->section('title');
echo 'Pegawai - Edit';
$this->endSection() ?>
<?= $this->section('content')?>
<div class="container pt-5" style="padding-left:300px;padding-right:300px">
    <form action="/pegawai/update" method="post">
    <div class="form-floating mt-0">
                <input type="text" class="form-control" id="nama" placeholder="nama" name="nama" value="<?= $user['nama_user']?>" required>
                <label for="nama">Nama Lengkap</label>
            </div>
            <div class="form-floating mt-2">
                <input type="text" class="form-control" id="username" placeholder="username" value="<?= $user['username']?>" disabled>
                <label for="username">Username</label>
            </div>
            <div class="form-floating mt-2">
                <input type="text" class="form-control" id="hp" placeholder="Nomor HP" name="hp" value="<?= $user['no_hp']?>" required>
                <label for="hp">Nomor HP</label>
            </div>
            <div class="form-floating mt-2">
                <input type="text" class="form-control" id="wa" placeholder="wa" name="wa" value="<?= $user['wa']?>" required>
                <label for="wa">Nomor WA</label>
            </div>
            <div class="form-floating mt-2">
                <input type="email" class="form-control" id="email" placeholder="email" name="email" value="<?= $user['email']?>" required>
                <label for="email">Email</label>
            </div>
            <div class="form-floating mt-2">
                <input type="text" class="form-control" id="pin" placeholder="PIN" name="pin" value="<?= $user['pin']?>" required>
                <label for="pin">PIN</label>
            </div>
            <div class="form-floating mt-2">
                <input type="text" class="form-control" id="pin" placeholder="jabatan" name="jabatan" value="<?= $user['jabatan']?>" required>
                <label for="pin">Jabatan</label>
            </div>
            <input type="hidden" value="<?= $user['id_user']; ?>" name="id">
            <div class="text-end mt-3">
                <a href="/pegawai"><button type="button" class="btn btn-secondary">Batal</button></a>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
<?= $this->endSection() ?>