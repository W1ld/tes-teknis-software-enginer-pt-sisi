<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.104.2">
    <title><?= $this->renderSection('title') ?></title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      .b-example-divider {
        height: 3rem;
        background-color: rgba(0, 0, 0, .1);
        border: solid rgba(0, 0, 0, .15);
        border-width: 1px 0;
        box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
      }

      .b-example-vr {
        flex-shrink: 0;
        width: 1.5rem;
        height: 100vh;
      }

      .bi {
        vertical-align: -.125em;
        fill: currentColor;
      }

      .nav-scroller {
        position: relative;
        z-index: 2;
        height: 2.75rem;
        overflow-y: hidden;
      }

      .nav-scroller .nav {
        display: flex;
        flex-wrap: nowrap;
        padding-bottom: 1rem;
        margin-top: -1px;
        overflow-x: auto;
        text-align: center;
        white-space: nowrap;
        -webkit-overflow-scrolling: touch;
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="assets/dist/css/headers.css" rel="stylesheet">
  </head>
  <body>
    
<main class="border-bottom">
  <div class="container">
    <header class="d-flex row flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-1">
      <div class="col-md-9">
        <ul class="nav nav-pills">
          <li class="nav-item"><a href="/home" class="nav-link <?= uri_string()=='home' ? 'active' : ''?>">Home</a></li>
          <li class="nav-item"><a href="/pegawai" class="nav-link <?= str_contains(uri_string(), 'pegawai') ? 'active' : ''?>">Pegawai</a></li>
          <li class="nav-item"><a href="/presensi" class="nav-link <?= uri_string()=='presensi' ? 'active' : ''?>">Presensi</a></li>
          <?php foreach($menu as $menu){?>
            <li class="nav-item"><a href="/u/<?= $menu['menu_link']?>" class="nav-link <?= uri_string()=='u/'.$menu['menu_link'] ? 'active' : ''?>"><?= $menu['menu_name']?></a></li>
          <?php }?>
          <?php if($_SESSION['level'] == "admin"){?>
          <li class="nav-item"><a href="/activity" class="nav-link <?= uri_string()=='activity' ? 'active' : ''?>">Activity Log</a></li>
          <?php }?>
          <li class="nav-item"><a href="#" class="nav-link"><button type="button" class="btn btn-success p-1" data-bs-toggle="modal" data-bs-target="#menumodal" data-whatever="@getbootstrap" style="margin-top:-5px">+Tambah Menu</button></a></li>
        </ul>
      </div>
      <div class="col-md-2 text-end">
        <div class="dropdown">
          <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="https://github.com/mdo.png" alt="mdo" width="32" height="32" class="rounded-circle"> <?= $this->renderSection('nama') ?>
          </a>
          <ul class="dropdown-menu text-small">
            <!-- <li><a class="dropdown-item" href="#">New project...</a></li>
            <li><a class="dropdown-item" href="#">Settings</a></li>
            <li><a class="dropdown-item" href="#">Profile</a></li>
            <li><hr class="dropdown-divider"></li> -->
            <li><a class="dropdown-item" href="/signout">Sign out</a></li>
          </ul>
        </div>
      </div>
    </header>
</main>
    <?= $this->renderSection('content') ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js" integrity="sha512-2rNj2KJ+D8s1ceNasTIex6z4HWyOnEYLVC3FigGOmyQCZc2eBXKgOxQmo3oKLHyfcj53uz4QMsRCWNbLd32Q1g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <footer class="py-3 mt-4 border-top">
      <p class="text-center text-muted">&copy; 2023 Wildan</p>
    </footer>
  </body>
</html>