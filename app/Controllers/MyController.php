<?php
 
namespace App\Controllers;
 
use CodeIgniter\Controller;

use App\Models\User_model;
use App\Models\Presensi_model;
use App\Models\userActivity_model;
use App\Models\MenuUser_model;
 
class MyController extends BaseController
{
    public function index()
    {
        $data['title']  = 'Sign in';
        $data['msg']    = 'Silahkan masukkan username dan password';
        return view('login/index.php', $data);
    }

    public function home()
    {
        $this->userActivity("akses menu home");
        $data['menu'] = $this->menu();
        return view('pages/home.php', $data);
    }

    public function pegawai()
    {
        $model = new User_model;
        $data['users'] = $model->getUser();
        $data['menu'] = $this->menu();
        $this->userActivity("akses menu pegawai");
        return view('pages/pegawai.php', $data);
    }

    public function loginAuth()
    {
        $session = session();
        $userModel = new User_model();
        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');
        helper(['form']);

        $data = $userModel->where('username', $username)->first();
        
        if($data){
            $pass = $data['password'];
            // $authenticatePassword = password_verify($password, $pass); // kudu atek password_hash($password, PASSWORD_DEFAULT) nang register
            // if($authenticatePassword){
            if($password === $pass){
                if ($data['id_jenis_user'] === 'mem') { // user biasa
                    $ses_data = [
                        'id_user' => $data['id_user'],
                        'nama_user' => $data['nama_user'],
                        'level' => 'member',
                        'isLoggedIn' => TRUE
                    ];
                } elseif ($data['id_jenis_user'] === 'adm') { // admin
                    $ses_data = [
                        'id_user' => $data['id_user'],
                        'nama_user' => $data['nama_user'],
                        'level' => 'admin',
                        'isLoggedIn' => TRUE
                    ];
                }
                $session->set($ses_data);
                $this->userActivity("login");
                return redirect()->to('/home');
            
            }else{
                $session->setFlashdata('msg', 'Password salah.');
                return redirect()->to('/');
            }
        }else{
            $session->setFlashdata('msg', "Username <b>$username</b> tidak ditemukan.");
            return redirect()->to('/');
        }
    }

    public function logout(){
        $session = session();
        $session->destroy();
        $this->userActivity("logout");
        return redirect()->to('/');
    }

    public function register(){
        $data['title']  = 'Sign up';
        return view('login/register.php', $data);
    }

    public function insert()
    {
        $model = new User_model;
        $id = date('YmdHi');
        $data = array(
            'id_user' => $id,
            'nama_user' => $this->request->getPost('nama'),
            'username' => $this->request->getPost('username'),
            'password' => $this->request->getPost('password'),
            'email' => $this->request->getPost('email'),
            'no_hp' => $this->request->getPost('hp'),
            'wa' => $this->request->getPost('wa'),
            'pin' => $this->request->getPost('pin'),
            'jabatan' => $this->request->getPost('jabatan'),
        );
        if(isset($_SESSION['id_user'])){
            $this->userActivity("menambah pegawai baru");
        }
        $model->insertUser($data);
        echo '<script>
                alert("Berhasil Menambah Data Pegawai Baru.");
                window.location="' . base_url('/pegawai') . '"
            </script>';
    }

    public function editPegawai($id)
    {
        $model = new User_model;
        $data['user'] = $model->getUser($id);
        $data['menu'] = $this->menu();
        $this->userActivity("akses menu edit pegawai $id");
        return view('pages/editPegawai.php', $data);
    }

    public function updatePegawai()
    {
        $model = new User_model;
        $id = $this->request->getPost('id');
        $data = array(
            'nama_user' => $this->request->getPost('nama'),
            'email' => $this->request->getPost('email'),
            'no_hp' => $this->request->getPost('hp'),
            'wa' => $this->request->getPost('wa'),
            'pin' => $this->request->getPost('pin'),
            'jabatan' => $this->request->getPost('jabatan'),
            'update_by' => session()->id_user,
        );
        $this->userActivity("mengubah data pegawai");
        $model->updateUser($data, $id);
        echo '<script>
                alert("Berhasil Mengubah Data Pegawai.");
                window.location="' . base_url('/pegawai') . '"
            </script>';
    }

    public function deletePegawai($id)
    {
        $model = new User_model;
        $getKaryawan = $model->getUser($id);
        if (isset($getKaryawan)) {
            if($id == session()->id_user)
                echo '<script>
                    alert("Gagal menghapus akun yang digunakan saat ini!");
                    window.location="' . base_url('pegawai') . '"
                </script>';
            $model->deleteUser($id);
            $this->userActivity("menghapus id_user $id");
            echo '<script>
                    alert("Selamat! Data berhasil terhapus.");
                    window.location="' . base_url('pegawai') . '"
                </script>';
        } else {
            echo '<script>
                    alert("Gagal menghapus!, ID karyawan ' . $id . ' Tidak ditemukan");
                    window.location="' . base_url('pegawai') . '"
                </script>';
        }
    }

    public function presensi($enum = false)
    {
        $model = new Presensi_model;
        if($enum !== false){
            $date = date('Y-m-d');
            $id = $_SESSION['id_user'];
            $data = array(
                'id_user' => $id,
                'keterangan' => $enum,
                'tanggal' => $date,
            );
            $model->insertData($data);
            session()->setFlashdata('msg', "Presensi berhasil disimpan!");
            $this->userActivity("melakukan presensi dengan status $enum");
            return redirect()->to('/presensi');
        }
        else {
            $data['user'] = $model->getData();
            $data['today'] = $model->getToday($_SESSION['id_user']);
            $data['menu'] = $this->menu();
            $this->userActivity("akses menu presensi");
            return view('pages/presensi.php', $data);
        }
    }

    public function userMenu($link){
        $data['menu'] = $this->menu();
        return view("pages/$link.php", $data);
    }

    public function activity()
    {
        $model = new userActivity_model;
        $data['data'] = $model->getData();
        $data['menu'] = $this->menu();
        return view('pages/activity.php', $data);
    }

    protected function userActivity($desc)
    {
        $model = new userActivity_model;
        $id = $_SESSION['id_user'];
        $data = array(
            'id_user' => $id,
            'discripsi' => $desc,
            'Status' => 'Aman',
            'create_by' => 'System',
        );
        // $model->insertData($data);
    }

    protected function menu(){
        $model = new MenuUser_model;
        $id = $_SESSION['id_user'];
        return $model->getData($id);
    }
}