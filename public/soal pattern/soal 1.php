<!DOCTYPE html>
<html>
<head>
	<!-- <title></title> -->
</head>
<body>
	<h1>SOAL 1</h1>
	<label for="dimension">Input:</label>
	<form method="post">
	<input type="number" name="input" min="3" max="9" value="<?php if(isset($_POST['input'])) echo $_POST['input'] ?>" autofocus>
	<br><br>
	<button id="submit">Submit</button>
	</form>
	<br><br>
	<?php if(isset($_POST['input'])){
		$input = isset($_POST['input']) ? $_POST['input'] : 1;
		$temp = 0;
		for ($i=0; $i < $input; $i++) {
			for ($j=$input; $j > 0; $j--) {
				if($j <= $i+1)
				echo "*";
				else echo "&nbsp";
			}
			echo '<br>';
		}
	}
	?>
</body>
</html>